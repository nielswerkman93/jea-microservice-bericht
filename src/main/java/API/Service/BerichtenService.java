package API.Service;

import Model.Bericht;

import java.util.List;

public interface BerichtenService {
    Bericht getById(int id);
    List<Bericht> getAll();
    Bericht add(Bericht bericht);
    Bericht update(Bericht bericht);
    void delete(Bericht bericht);

}
