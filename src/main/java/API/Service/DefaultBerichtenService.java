package API.Service;

import Model.Bericht;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;
import java.util.List;

@ApplicationScoped
public class DefaultBerichtenService implements BerichtenService {

    @PersistenceContext(unitName = "PU")
    private EntityManager entityManager;

    @Override
    public Bericht getById(int id) {
        return entityManager.createQuery("SELECT u FROM Bericht u WHERE u.id=:id", Bericht.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<Bericht> getAll() {
        return entityManager.createQuery("SELECT u FROM Bericht u", Bericht.class).getResultList();
    }

    @Override
    @Transactional
    public Bericht add(Bericht bericht) {
        try{
            entityManager.persist(bericht);
        }catch (Exception e){
            e.printStackTrace();
        }
        return bericht;
    }

    @Override
    @Transactional
    public Bericht update(Bericht bericht) {
        try{
            entityManager.merge(bericht);
        }catch (Exception e){
            e.printStackTrace();
        }
        return bericht;
    }

    @Override
    @Transactional
    public void delete(Bericht bericht) {
        try{
            entityManager.remove(entityManager.contains(bericht) ? bericht : entityManager.merge(bericht));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
