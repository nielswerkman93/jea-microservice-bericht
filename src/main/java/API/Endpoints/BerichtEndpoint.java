package API.Endpoints;

import API.Service.BerichtenService;
import Model.Bericht;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Produces(MediaType.APPLICATION_JSON)
@Path("/bericht")
public class BerichtEndpoint {

    @Inject
    private BerichtenService service;

    @Context
    UriInfo uriInfo;

    @GET
    @Path("{id}")
    public Response getBericht(@PathParam("id") int id){
        Bericht bericht;

        try{
            bericht = service.getById(id);
        }catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), e.toString()).build();
        }

        return Response.ok()
                .entity(bericht)
                .link(uriInfo.getAbsolutePathBuilder().build(id), "self")
                .build();
    }
    @GET
    public Response getAll(){
        List<Bericht> berichten;

        try{
            berichten = service.getAll();
        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), e.toString()).build();
        }
        return Response.ok()
                .entity(berichten)
                .link(uriInfo.getAbsolutePathBuilder().build(), "self")
                .build();
    }

    @POST
    public Response add(@Valid Bericht bericht){
        try{
            service.add(bericht);
        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), e.toString()).build();
        }
        return Response.status(Response.Status.CREATED.getStatusCode()).build();
    }

    @PUT
    public Response update(@Valid Bericht bericht){
        try{
            service.update(bericht);
        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), e.toString()).build();
        }
        return Response.status(Response.Status.OK.getStatusCode()).build();
    }

    @DELETE
    public Response delete(@Valid Bericht bericht){
        try{
            service.delete(bericht);
        } catch (Exception e){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), e.toString()).build();
        }
        return Response.ok().build();
    }
}
