package Model;

import com.google.gson.Gson;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class BerichtEncoder implements Encoder.Text<Bericht> {
    private static Gson gson = new Gson();

    @Override
    public String encode(Bericht bericht) throws EncodeException {
        return gson.toJson(bericht);
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
