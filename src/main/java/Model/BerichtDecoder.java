package Model;

import com.google.gson.Gson;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

public class BerichtDecoder implements Decoder.Text<Bericht> {
    private static Gson gson = new Gson();

    @Override
    public Bericht decode(String s) throws DecodeException {
        return gson.fromJson(s, Bericht.class);
    }

    @Override
    public boolean willDecode(String s) {
        return (s != null);
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
