package Model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Bericht {

    @Id
    @GeneratedValue
    private int id;

    private String text;
    private String title;
    private Date date;

    private int userId;


    public Bericht() {
    }

    public Bericht(int id, String text) {
        this.id = id;
        this.text = text;
    }

    public Bericht(String text, Date date, int userId) {
        this.text = text;
        this.date = date;
        this.userId = userId;
    }

    //region Getters & Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    //endregion

}
