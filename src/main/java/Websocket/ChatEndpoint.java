package Websocket;

import Model.Bericht;
import Model.BerichtDecoder;
import Model.BerichtEncoder;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

@ServerEndpoint(value = "/chat/{username}", decoders = BerichtDecoder.class, encoders = BerichtEncoder.class)
public class ChatEndpoint {

    private Session session;
    private static Set<ChatEndpoint> chatEndpoints
            = new CopyOnWriteArraySet<>();
    private static HashMap<String, String> users = new HashMap<>();


    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username) throws IOException {
        // Get session and WebSocket connection

        this.session = session;
        chatEndpoints.add(this);
        users.put(session.getId(), username);

        String message = "Hallo";
//        broadcast(null);
    }

    @OnMessage
    public void onMessage(Session session, Bericht bericht) throws IOException, EncodeException {
        // Handle new messages

        broadcast(bericht);
    }

    @OnClose
    public void onClose(Session session) throws IOException {
        // WebSocket connection closes

        chatEndpoints.remove(this);
//        broadcast(null);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        // Do error handling here
    }

    private static void broadcast(Bericht bericht) {

        chatEndpoints.forEach(endpoint -> {
            synchronized (endpoint) {
                try {
                    endpoint.session.getBasicRemote().
                            sendObject(bericht);
                } catch (IOException | EncodeException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
